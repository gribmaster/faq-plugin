(function($){
	'use strict';

	class GribasAccordionItem {
		constructor(
			index,
			item,
			itemHead,
			itemBody,
			listener = 'click',
			onChange,
			onShow,
			onHide,
			showItem
		) {
			this.item = item;
			this.hidden = true;
			this.showItem = showItem;
			this.itemHead = itemHead;
			this.itemBody = itemBody;
			this.listener = listener;
			this.index = index;
			this.onChange = onChange;
			this.onHide = onHide;
			this.onShow = onShow;

			let a = new GribasAccordionListener(
				this.item,
				this.hidden,
				this.index,
				this.showItem,
				this.itemHead,
				this.itemBody,
				this.onChange,
				this.onShow,
				this.onHide
			);
			a.tabClick(this.item);
			a.openOnInit(this.item);
			//console.log(a.allItemsCount)
		}
	}


	class GribasAccordionAction {
		constructor(
			item,
			hidden,
			onChange,
			onShow,
			onHide,
		) {
			this.item = item;
			this.hidden = hidden;
			this.onChange = onChange;
			this.onHide = onHide;
			this.onShow = onShow;

		}
		hide(itemBody) {
			$(itemBody).slideUp();
			$(this.item).removeClass('expanded');
			this.hidden = true;
			this.onChange($(this.item));
			this.onHide($(this.item));
		}
		show(itemBody) {
			$(itemBody).slideDown();
			$(this.item).addClass('expanded');
			this.hidden = false;
			this.onChange($(this.item));
			this.onShow($(this.item));
		}
	}

	class GribasAccordionListener {
		constructor(
			item,
			hidden,
			index,
			showItem,
			itemHead,
			itemBody,
			onChange,
			onShow,
			onHide
		) {
			this.hidden = hidden;
			this.onChange = onChange;
			this.onHide = onHide;
			this.onShow = onShow;
			this.showItem = showItem;
			this.item = item;
			this.index = index;
			this.itemBody = itemBody;
			this.itemHead = itemHead;

			this.call = new GribasAccordionAction(
				this.item,
				this.hidden,
				this.onChange,
				this.onShow,
				this.onHide
			);
		}
		openOnInit(item) {
			let self = this;
			let call = this.call;

			$(this.showItem).each(function(i, v){
				console.log(v)
				if (v > 0) {
					self.index === v-1 ? call.show(item.find(self.itemBody)) : true;
				}
			})
		}
		tabClick(item) {
			let self = this;
			let call = this.call;

			$(item).find(self.itemHead).on('click', function(event) {
				call.hidden ? call.show(item.find(self.itemBody)) : call.hide(item.find(self.itemBody));
			});			
		}
	}

	$.fn.Harmony = function(opt){
		let settings = $.extend({
			head: '',
			body: '',
			listener: 'click', //need to do
			showItem: [],
			duration: 400, //need to do
			type: 'slide', //need to do
			afterLoad: function(item){},
			onChange: function(item){},
			onShow: function(item){},
			onHide: function(item){},
		}, opt);
		
	 	let item = this;
		$.each(item, function (index) {
			//console.log(index)
			let a = new GribasAccordionItem(
				index,
				$(this),
				settings.head,
				settings.body,
				settings.listener,
				settings.onChange,
				settings.onShow,
				settings.onHide,
				settings.showItem
			);
			console.log(a)
		})
		settings.afterLoad();

		return this;
	};
}(jQuery));
